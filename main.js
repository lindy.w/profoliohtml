/* =================================
------------------------------------
	Woon Him WONG - Portfolio
	Version: 1.0
 ------------------------------------ 
 ====================================*/
'use strict';


	/*------------------
		Popup
	--------------------*/
		$('.portfolio-item').magnificPopup({
		type: 'image',
		mainClass: 'img-popup-warp',
		removalDelay: 400,
	});

	